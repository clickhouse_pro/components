package browscap

import (
	"bitbucket.org/clickhouse_pro/components/stacktrace"
	"github.com/digitalcrab/browscap_go"
	"github.com/rs/zerolog/log"
	"os"
)

//InitBrowsCap download browscap.ini from http://browscap.org/ and read it
func InitBrowsCap() {
	iniFile, lastVersion, needDownload := getBrowscapLastVersion()

	if needDownload || lastVersion != browscap_go.InitializedVersion() {
		downloadBrowserCap(lastVersion, iniFile)
	}
}

func downloadBrowserCap(lastVersion string, iniFile string) {
	log.Printf("Start downloading BROWSECAP lastVersion %q", lastVersion)
	newFile := iniFile + ".new"
	if err := browscap_go.DownloadFile(newFile); err != nil {
		log.Fatal().Err(err).Msgf("Download error newFile=%s", newFile)
	}
	log.Printf("Initializing BROWSECAP with lastVersion %s", lastVersion)
	if err := browscap_go.InitBrowsCap(newFile, true); err != nil {
		log.Fatal().Err(err).Str("newFile", newFile).Msg("browscap_go.InitBrowsCap error")
	}
	newFileVersion := browscap_go.InitializedVersion()
	if lastVersion != newFileVersion {
		log.Fatal().
			Str("newFile", newFile).Str("lastVersion", lastVersion).Str("newFileVersion", newFileVersion).
			Msg("New file have wrong version")
	} else {
		if err := os.Rename(newFile, iniFile); err != nil {
			stacktrace.DumpErrorStackAndExit(err)
		}
	}
}

func getBrowscapLastVersion() (iniFile string, lastVersion string, needDownload bool) {
	iniFile = "/tmp/browscap_go_browscap.ini"
	if tmp := os.Getenv("TEMP"); tmp != "" {
		iniFile = os.Getenv("TEMP") + "/browscap_go_browscap.ini"
	}
	lastVersion, err := browscap_go.LastVersion()
	if err != nil {
		log.Fatal().Str("iniFile", iniFile).Err(err).Msg("browscap_go.LastVersion error")
	}
	needDownload = false
	if _, err = os.Stat(iniFile); os.IsNotExist(err) {
		needDownload = true
	} else if err != nil {
		log.Fatal().Str("iniFile", iniFile).Err(err).Msg("os.Stat error")
	} else {
		err = browscap_go.InitBrowsCap(iniFile, true)
		if err != nil {
			log.Fatal().Str("iniFile", iniFile).Err(err).Msg("browscap_go.InitBrowsCap error")
		}
		log.Info().Str("iniFile", iniFile).Msg("initializing data from http://browscap.org")
	}
	return iniFile, lastVersion, needDownload
}
