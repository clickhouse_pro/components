package helpers

import "time"

//BeginOfDay truncate t to begin of date in t.Location() timezone
func BeginOfDay(t time.Time) time.Time {
	year, month, day := t.Date()
	return time.Date(year, month, day, 0, 0, 0, 0, t.Location())
}

//EndOfDay truncate t to end of date in t.Location() timezone
func EndOfDay(t time.Time) time.Time {
	year, month, day := t.Date()
	return time.Date(year, month, day, 23, 59, 59, 999999999, t.Location())
}
