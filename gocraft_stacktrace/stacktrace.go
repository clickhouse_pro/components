package gocraft_stacktrace

import (
	"github.com/gocraft/web"
	"github.com/rs/zerolog/log"
	"net/http"
	"runtime"
)

//LogErrorMiddleware catch all panic() in WebHandlers and log stacktrace
func LogErrorMiddleware(rw web.ResponseWriter, req *web.Request, next web.NextMiddlewareFunc) {
	defer func() {
		if err := recover(); err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			const size = 4096
			stack := make([]byte, size)
			stack = stack[:runtime.Stack(stack, false)]
			_, filePath, line, _ := runtime.Caller(5)

			log.Error().Err(err.(error)).Str("file", filePath).Int("line", line).Str("stacktrace", string(stack)).Msg("logErrorMiddleware executed")
		}
	}()

	next(rw, req)
}
