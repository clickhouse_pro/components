package zkpool

import (
	"bitbucket.org/clickhouse_pro/components/cfg"

	"github.com/rs/zerolog/log"
	"github.com/samuel/go-zookeeper/zk"
	"path"
	"sort"
	"time"
)

//InitZookeeper connect to Zookeeper.Hosts and get all available clickhouse hosts
func InitZookeeper(config *cfg.ConfigType) {
	if config.Zookeeper.Hosts != nil && len(config.Zookeeper.Hosts) > 0 {
		zkConn, _, err := zk.Connect(config.Zookeeper.Hosts, time.Second*3)
		if err != nil {
			log.Fatal().Msgf("Error Zookeeper connection to %v: %v", config.Zookeeper.Hosts, err)
		}
		zkConn.SetLogger(&log.Logger)
		if config.Zookeeper.Username != "" {
			err = zkConn.AddAuth("digest", []byte(config.Zookeeper.Username+":"+config.Zookeeper.Password))
			if err != nil {
				log.Fatal().Msgf(
					"Error Zookeeper digest authorization %s to %v: %v",
					config.Zookeeper.Username+":"+config.Zookeeper.Password, config.Zookeeper.Hosts, err,
				)
			}
		}
		zkData, err := childrenRecursiveInternal(zkConn, config.Zookeeper.Path, "")
		if err != nil {
			log.Fatal().Msgf(
				"Error Zookeeper get child subkeys %s from %v: %v",
				config.Zookeeper.Path, config.Zookeeper.Hosts, err,
			)
		}
		// @todo DEV/20m need implementation for get all currently live clickhouse hosts from zookeeper
		log.Debug().Msgf("zkData=%v", zkData)
	}
}

func childrenRecursiveInternal(connection *zk.Conn, basePath string, incrementalPath string) ([]string, error) {
	children, _, err := connection.Children(basePath)
	if err != nil {
		return children, err
	}
	sort.Sort(sort.StringSlice(children))
	var recursiveChildren []string
	for _, child := range children {
		incrementalChild := path.Join(incrementalPath, child)
		recursiveChildren = append(recursiveChildren, incrementalChild)
		log.Debug().Msgf("incremental child: %+v", incrementalChild)
		var incrementalChildren []string
		incrementalChildren, err = childrenRecursiveInternal(connection, path.Join(basePath, child), incrementalChild)
		if err != nil {
			return children, err
		}
		recursiveChildren = append(recursiveChildren, incrementalChildren...)
	}
	return recursiveChildren, err
}
